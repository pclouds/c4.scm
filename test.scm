(import (scheme base)
        (srfi 4) (srfi 69) (srfi 95)
        (gauche base) (gauche test)
        (c4))

(test-start "c4")

(test-section "tokenizer")

(define (test-tokenizer name expect input)
  (define (tokenize string)
    (with-input-from-string string
      (lambda ()
        (reverse
         (let loop ([output '()]
                    [token (read-token)])
           (if (eof-object? token)
               output
               (loop (cons token output)
                     (read-token))))))))

  (test* name expect (tokenize input)))


(test-tokenizer "empty" '() "")

(test-tokenizer "ignore proprocessor directives" '() "#abc def")
(test-tokenizer "ignore proprocessor directives" '((num . 1)) "#abc def\n1")

(test-tokenizer "zero" '((num . 0)) "0")
(test-tokenizer "one digit decimal" '((num . 1)) "1")
(test-tokenizer "decimal number" '((num . 10239)) "10239")
;(test-tokenizer "broken hexa number" '() "0x")
(test-tokenizer "hexa number" '((num . #x19af)) "0x19af")
(test-tokenizer "octal number" '((num . #o1027)) "01027")

(test-tokenizer "char" '((char . 97)) "'a'")
(test-tokenizer "string" '((str . "abc")) "\"abc\"")

;(test-tokenizer "assignment" '(assign) "=")
(test-tokenizer "assignment" '(assign (num . 1)) "= 1")
(test-tokenizer "equal" '(eq (num . 1)) "== 1")

(test-section "expression parser")

(define (test-expr name expected-type code data
                   context input :optional (syms '()) (start-data 8))
  (let ([env (%make-env (open-output-string)
                        (list 0)
                        (make-hash-table))])
    (hash-table-merge! (env-syms env)
                       (alist->hash-table
                        (map (lambda (x)
                               (let ([id (car x)]
                                     [attr (cdr x)])
                                 (cons id (make-sym (car attr)
                                                    (cadr attr)
                                                    (caddr attr)))))
                             syms)))
    (set! (env-data env) (list start-data))
    (with-input-from-string input
      (lambda ()
        (let ([type (expr env context (read-token))])
          (test* (string-append name " - type")
                 (cons expected-type (eof-object)) type)
          (test* (string-append name " - code")
                 code (port->sexp-list
                       (open-input-string
                        (get-output-string
                         (env-program env)))))
          (test* (string-append name " - data")
                 data (env-data env)))))))

(test-expr "one number"
           'int '((imm . 12)) '(8)
           'assign "12")

(test-expr "one char"
           'char '((imm . 97)) '(8)
           'assign "'a'")

(test-expr "one string"
           'ptr '((imm . 8)) '(12 (4 . "abc"))
            'assign "\"abc\"")

;; (test-expr "string concatenation"
;;            'ptr '((imm . 4)) '(8 (8 . "abcdef"))
;;             'assign "\"abc\" \"def\"")

(test-expr "exit call"
           'int '(exit) '(8)
           'assign "exit()" '((exit . (sys exit int))))

(test-expr "func call, no param"
           'int '((jsr . 12)) '(8)
           'assign "func()" '((func . (func 12 int))))

(test-expr "func call, one param"
           'int '((imm . 1) psh (jsr . 12) (adj . 1)) '(8)
           'assign "func(1)" '((func . (func 12 int))))

(test-expr "func call, two params"
           'int '((imm . 1) psh (imm . 2) psh (jsr . 12) (adj . 2)) '(8)
           'assign "func(1,2)" '((func . (func 12 int))))

(test-expr "one global int"
           'int '((imm . 4) li) '(8)
           'assign "abc " '((abc . (global 4 int))))

(test-expr "one global char"
           'char '((imm . 4) lc) '(8)
           'assign "abc " '((abc . (global 4 char))))

(test-expr "one enum"
           'int '((imm . 4)) '(8)
           'assign "abc " '((abc . (num 4 int))))

(test-expr "one local int"
           'int '((lea . 4) li) '(8)
           'assign "abc " '((abc . (local 4 int))))

(test-expr "one local char"
           'char '((lea . 4) lc) '(8)
           'assign "abc " '((abc . (local 4 char))))

(test-expr "! int"
           'int '((imm . 123) psh (imm . 0) eq) '(8)
           'assign "!123" '())

(test-expr "~ int"
           'int '((imm . 123) psh (imm . -1) xor) '(8)
           'assign "~123" '())

(test-expr "- int"
           'int '((imm . -123)) '(8)
           'assign "-123" '())

(test-expr "- sym"
           'int '((imm . -1) psh (imm . 4) li mul) '(8)
           'assign "-abc " '((abc . (global 4 int))))

(test-expr "int || int"
           'int '((imm . 1) (bnz . 1) (imm . 2)) '(8)
           'assign "1||2" '())

(test-expr "int && int"
           'int '((imm . 1) (bz . 1) (imm . 2)) '(8)
           'assign "1&&2" '())

(test-expr "int | int"
           'int '((imm . 1) psh (imm . 2) or) '(8)
           'assign "1|2" '())

(test-expr "int ^ int"
           'int '((imm . 1) psh (imm . 2) xor) '(8)
           'assign "1^2" '())

(test-expr "int & int"
           'int '((imm . 1) psh (imm . 2) and) '(8)
           'assign "1&2" '())

(test-expr "int == int"
           'int '((imm . 1) psh (imm . 2) eq) '(8)
           'assign "1==2" '())

(test-expr "int != int"
           'int '((imm . 1) psh (imm . 2) ne) '(8)
           'assign "1!=2" '())

(test-expr "int < int"
           'int '((imm . 1) psh (imm . 2) lt) '(8)
           'assign "1<2" '())

(test-expr "int > int"
           'int '((imm . 1) psh (imm . 2) gt) '(8)
           'assign "1>2" '())

(test-expr "int <= int"
           'int '((imm . 1) psh (imm . 2) le) '(8)
           'assign "1<=2" '())

(test-expr "int >= int"
           'int '((imm . 1) psh (imm . 2) ge) '(8)
           'assign "1>=2" '())

(test-expr "int << int"
           'int '((imm . 1) psh (imm . 2) shl) '(8)
           'assign "1<<2" '())

(test-expr "int >> int"
           'int '((imm . 1) psh (imm . 2) shr) '(8)
           'assign "1>>2" '())

(test-expr "int + int"
           'int '((imm . 1) psh (imm . 2) add) '(8)
           'assign "1+2" '())

(test-expr "sym + sym"
           'int '((imm . 4) li psh (imm . 8) li add) '(8)
           'assign "abc+def " '((abc . (global 4 int))
                                (def . (global 8 int))))

(test-expr "sym + int"
           'int '((imm . 4) li psh (imm . 2) add) '(8)
           'assign "abc+2" '((abc . (global 4 int))))

(test-expr "int + sym"
           'int '((imm . 1) psh (imm . 4) li add) '(8)
           'assign "1+abc " '((abc . (global 4 int))))

(test-expr "int - int"
           'int '((imm . 1) psh (imm . 2) sub) '(8)
           'assign "1-2" '())

(test-expr "int * int"
           'int '((imm . 1) psh (imm . 2) mul) '(8)
           'assign "1*2" '())

(test-expr "int / int"
           'int '((imm . 1) psh (imm . 2) div) '(8)
           'assign "1/2" '())

(test-expr "int % int"
           'int '((imm . 1) psh (imm . 2) mod) '(8)
           'assign "1%2" '())

(test-expr "int * int + int"
           'int '((imm . 1) psh (imm . 2) mul psh (imm . 3) add) '(8)
           'assign "1*2+3" '())

(test-expr "int + int * int"
           'int '((imm . 1) psh (imm . 2) psh (imm . 3) mul add) '(8)
           'assign "1+2*3" '())

(test-expr "(int + int) * int"
           'int '((imm . 1) psh (imm . 2) add psh (imm . 3) mul) '(8)
           'assign "(1+2)*3" '())

(test-expr "var = int"
           'int '((imm . 4) psh (imm . 1) si) '(8)
           'assign "a=1" '((a . (global 4 int))))

(test-expr "var = (int + int) * int + int"
           'int
           '((imm . 123) psh
             (imm . 1) psh
             (imm . 2) add psh
             (imm . 3) mul psh
             (imm . 4) add
             si)
           '(8)
           'assign "a=(1+2)*3+4" '((a . (global 123 int))))

(test-section "statement parser")

(define (test-stmt name code input :optional (syms '()) (start-data 8))
  (let ([env (%make-env (open-output-string)
                        (list 0)
                        (make-hash-table))])
    (hash-table-merge! (env-syms env)
                       (alist->hash-table
                        (map (lambda (x)
                               (let ([id (car x)]
                                     [attr (cdr x)])
                                 (cons id (make-sym (car attr)
                                                    (cadr attr)
                                                    (caddr attr)))))
                             syms)))
    (set! (env-data env) (list start-data))
    (with-input-from-string input
      (lambda ()
        (stmt env (read-token))
        (test* (string-append name " - code")
               code (port->sexp-list
                     (open-input-string
                      (get-output-string
                       (env-program env)))))))))

(test-stmt "empty statements"
           '()
           ";;;;}" '())

(test-stmt "empty return"
           '(lev)
           "return;}" '())

(test-stmt "return int"
           '((imm . 1) lev)
           "return 1;}" '())

(test-stmt "var = int;"
           '((imm . 123) psh (imm . 1) si)
           "a=1;}" '((a . (global 123 int))))

(test-section "toplevel parser")

(define (test-toplevel name code data syms
                       input :optional (initial-syms '()))
  (define (syms->list syms)
    (map
     (lambda (x)
       (let ([sym (cdr x)])
         (list (car x)
               (list (sym-class sym)
                     (sym-addr sym)
                     (sym-type sym)))))
     (sort (hash-table->alist syms)
           (lambda (a b)
             (string<? (symbol->string a)
                       (symbol->string b)))
           car)))

  (let ([env (%make-env (open-output-string)
                        (list 0)
                        (make-hash-table))])
    (hash-table-merge! (env-syms env)
                       (alist->hash-table
                        (map (lambda (x)
                               (let ([id (car x)]
                                     [attr (cdr x)])
                                 (cons id (make-sym (car attr)
                                                    (cadr attr)
                                                    (caddr attr)))))
                             initial-syms)))
    (with-input-from-string input
      (lambda ()
        (toplevel env)
        (test* (string-append name " - syms")
               syms (syms->list (env-syms env)))
        (test* (string-append name " - code")
               code (port->sexp-list
                     (open-input-string
                      (get-output-string
                       (env-program env)))))
        (test* (string-append name " - data")
               data (env-data env))))))

(test-toplevel "one global var"
               '() '(4 (4 . 0)) '((abc (global 0 int)))
               "int abc;")

(test-toplevel "two global vars, one statement"
               '() '(8 (4 . 0) (4 . 0)) '((abc (global 0 int))
                                          (def (global 4 int)))
               "int abc, def;")

(test-toplevel "empty func, no param"
               '((ent . 0) lev) '(0) '((func (function 0 int)))
               "int func() {}")

(test-toplevel "func, no param"
               '((ent . 0) (imm . 0) psh (imm . 1) si lev)
               '(4 (4 . 0))
               '((a (global 0 int)) (func (function 0 int)))
               "int a;int func() {a=1;}")

(test-toplevel "func, exit, no param. local var"
               '((ent . 1) (lea . 0) psh (imm . 1) si (adj . 1) lev)
               '(0)
               '((exit (sys exit int)) (func (function 0 int)))
               "int func() {int a;a = 1;}" '((exit . (sys exit int))))

(test-toplevel "func, exit, no param"
               '((ent . 0) exit lev)
               '(0)
               '((exit (sys exit int)) (func (function 0 int)))
               "int func() {exit();}" '((exit . (sys exit int))))

(test-section "execution")

(define (test-run name expect isns :optional (data-size 32))
  (test* name expect (run isns 0 (make-s32vector data-size))))

(test-run "lea" 124 '((ent . 0) (lea . 1) psh exit) 32)
(test-run "imm/psh" 10 '((imm . 10) psh exit))
(test-run "jmp" 10 '((jmp . 2) invalid (imm . 10) psh exit))
(test-run "jsr" 1 '((jsr . 2) invalid exit))
(test-run "bz jumps" 10 '((imm . 0) (bz . 3) invalid (imm . 10) psh exit))
(test-run "bz skips" 10 '((imm . 1) (bz . 5) (imm . 10) psh exit invalid))

(test-run "or" 15 '((imm . 3) psh (imm . 12) or psh exit))
(test-run "xor" 3 '((imm . 15) psh (imm . 12) xor psh exit))
(test-run "and" 12 '((imm . 15) psh (imm . 12) and psh exit))
(test-run "eq" 1 '((imm . 15) psh (imm . 15) eq psh exit))
(test-run "eq" 0 '((imm . 15) psh (imm . 14) eq psh exit))
(test-run "ne" 0 '((imm . 15) psh (imm . 15) ne psh exit))
(test-run "ne" 1 '((imm . 15) psh (imm . 14) ne psh exit))
(test-run "lt" 1 '((imm . 14) psh (imm . 15) lt psh exit))
(test-run "lt" 0 '((imm . 14) psh (imm . 14) lt psh exit))
(test-run "gt" 1 '((imm . 14) psh (imm . 13) gt psh exit))
(test-run "gt" 0 '((imm . 14) psh (imm . 14) gt psh exit))

(test-end :exit-on-failure #t)
