(define current-line 1)

(define-constant *precedence-list*
  (list 'assign 'cond 'lor 'land 'or 'xor 'and 'eq 'ne 'lt 'gt 'le 'ge
        'shl 'shr 'add 'sub 'mul 'div 'mod 'inc 'dec 'brak))

(define-record-type sym #t #f
  class addr type)

(define-record-type env %make-env #f
  (program) (data) (syms))

(define (make-env)
  (let ([env (%make-env (open-output-string)
                        (list 0)
                        (make-hash-table))])
    (let loop ([ht (env-syms env)]
               [sys-list '((exit int))])
      (hash-table-set! ht (caar sys-list)
                       (make-sym 'sys
                                 (caar sys-list)
                                 (cdar sys-list)))
      (if (pair? (cdr sys-list))
          (loop ht (cdr sys-list))))
    env))

(define (round-up-four x)
  (case (modulo x 4)
    [(0) x]
    [(1) (+ x 3)]
    [(2) (+ x 2)]
    [(3) (+ x 1)]))

(define (env-add-string! env str)
  (let* ([len (round-up-four (string-length str))]
         [offset (car (env-data env))]
         [total (+ len offset)]
         [data (cdr (env-data env))])
    (env-data-set! env (cons total (cons (cons len str) data)))
    offset))

(define (env-add-int! env value)
  (let* ([len 4]
         [offset (car (env-data env))]
         [total (+ len offset)]
         [data (cdr (env-data env))])
    (env-data-set! env (cons total (cons (cons len value) data)))
    offset))

;;; Read from input port and produce eof or one token, which is a
;;; symbol, or (type . value). Supported types are 'str, 'char and
;;; 'num
(define (read-token)
  (define (consume-line)
    (let loop ([ch (peek-char)])
      (unless (or (eof-object? ch) (char=? ch #\newline))
        (begin
          (read-char)
          (loop (read-char))))))

  (define (read-and-return ch)
    (read-char)
    ch)

  (define (read-and-peek)
    (read-char)
    (peek-char))

  (define (char-<=>? ch start stop)
    (and (char>=? ch start) (char<=? ch stop)))

  (define (char-ci-<=>? ch start stop)
    (and (char-ci>=? ch start) (char-ci<=? ch stop)))

  (define (char-symbol? ch)
    (or (char-ci-<=>? ch #\a #\z)
        (char=? ch #\_)))

  (define (char-newline? ch)
    (char=? ch #\newline))

  (define (read-number ch)
    (define (read-decimal val)
      (let loop ([val val]
                 [ch (peek-char)])
        (cond
         [(eof-object? ch) val]
         [(char-<=>? ch #\0 #\9)
          (loop (+ (* val 10)
                   (digit->integer ch))
                (read-and-peek))]
         [else val])))

    (define (read-octal val next)
      (let loop ([val val]
                 [ch next])
        (cond
         [(eof-object? ch) val]
         [(char-<=>? ch #\0 #\7)
          (loop (+ (* val 8)
                   (digit->integer ch))
                (read-and-peek))]
         [else val])))

    (define (read-hexadecimal val next)
      (let loop ([val val]
                 [ch next])
        (cond
         [(eof-object? ch) val]
         [(or (char-<=>? ch #\0 #\9)
              (char-ci-<=>? ch #\a #\f))
          (loop (+ (* val 16)
                   (digit->integer ch 16))
                (read-and-peek))]
         [else val])))

    (let ([val (digit->integer ch)])
      (if (positive? val)
          (read-decimal val)
          (let ([next (peek-char)])
            (cond
             [(eof-object? next) 0]
             [(char-ci=? next #\x) (read-hexadecimal val
                                                     (read-and-peek))]
             [else (read-octal val next)])))))

  (define (read-symbol ch)
    (let ([sym (open-output-string)])
      (write-char ch sym)
      (let loop ([ch (peek-char)])
        (if (or (char-ci-<=>? ch #\a #\z)
                (char-<=>? ch #\0 #\9)
                (char=? ch #\_))
            (begin
              (write-char ch sym)
              (loop (read-and-peek)))))
      (string->symbol (get-output-string sym))))

  (let loop ([ch (read-char)])
    (cond
     [(eof-object? ch) ch]
     [(char-symbol? ch) (cons 'sym (read-symbol ch))]
     [(char-numeric? ch) (cons 'num (read-number ch))]
     [(char-newline? ch) (begin
                           (set! current-line (+ current-line 1))
                           (loop (read-char)))]
     [(char-whitespace? ch) (loop (read-char))]
     [else
      (case ch
        [(#\#) (begin
                 (consume-line)
                 (loop (read-char)))]
        [(#\/) (let ([next (peek-char)])
                 (if (char=? next #\/)
                     (begin
                       (consume-line)
                       (loop (read-char)))
                     'div))]
        [(#\') (let ([val (let ([ch (read-char)])
                            (if (char=? ch #\\)
                                (let ([next (read-char)])
                                  (if (char=? next #\n)
                                      #\newline
                                      next))
                                ch))])
                 (unless (char=? (read-char) #\')
                   (error "invalid character"))
                 (cons 'char (char->integer val)))]
        [(#\") (let ([str (open-output-string)])
                 (let loop-str ([ch (read-char)])
                   (unless (or (eof-object? ch) (char=? ch #\"))
                     (begin
                       (write-char (if (char=? ch #\\)
                                       (let ([next (read-char)])
                                         (if (char=? next #\n)
                                             #\newline
                                             next))
                                       ch)
                                   str)
                       (loop-str (read-char)))))
                 (cons 'str (get-output-string str)))]
        [(#\=) (if (char=? (peek-char) #\=)
                   (read-and-return 'eq)
                   'assign)]
        [(#\+) (if (char=? (peek-char) #\+)
                   (read-and-return 'inc)
                   'add)]
        [(#\-) (if (char=? (peek-char) #\-)
                   (read-and-return 'dec)
                   'sub)]
        [(#\!) (if (char=? (peek-char) #\=)
                   (read-and-return 'ne)
                   'not)]
        [(#\<) (let ([next (peek-char)])
                 (cond
                  [(char=? next #\=) (read-and-return 'le)]
                  [(char=? next #\<) (read-and-return 'shl)]
                  [else 'lt]))]
        [(#\>) (let ([next (peek-char)])
                 (cond
                  [(char=? next #\=) (read-and-return 'ge)]
                  [(char=? next #\>) (read-and-return 'shr)]
                  [else 'gt]))]
        [(#\|) (if (char=? (peek-char) #\|)
                   (read-and-return 'lor)
                   'or)]
        [(#\&) (if (char=? (peek-char) #\&)
                   (read-and-return 'land)
                   'and)]
        [(#\^) 'xor]
        [(#\%) 'mod]
        [(#\*) 'mul]
        [(#\[) 'brak]
        [(#\?) 'cond]
        [(#\~) 'complement]
        [(#\;) 'eos]
        [(#\{) 'begin]
        [(#\}) 'end]
        [(#\() 'group]
        [(#\)) 'endgroup]
        [(#\]) 'endbrak]
        [(#\,) 'comma]
        [(#\:) 'colon]
        [else (error "unrecognized char" ch)])])))

;;; Process the given token (and following ones from input port if
;;; necessary) and produce opcodes to the port from
;;; (env-program). Return a pair of type and the next token.
(define (expr env context token)
  (define (token-type token)
    (if (pair? token)
        (car token)
        token))

  (define (token-type=? token type)
    (eq? (token-type token) type))

  (define (token-sym=? token sym)
    (and (token-type=? token 'sym)
         (eq (cdr token) sym)))

  (define (gen-imm val) (write (cons 'imm val) (env-program env)))
  (define (gen-op2 op val) (write (cons op val) (env-program env)))
  (define (gen-op op)
    (write op (env-program env))
    ;; work around the fact that we don't have proper sexp stream,
    ;; outputing two consecutive symbols will have no space between
    ;; them
    (write-char #\space (env-program env)))

  (define (gen-lhs token)
    (if (eof-object? token)
        (error "unexpected eof in the expression"))
    (case (token-type token)
      [(num) (begin
               (gen-imm (cdr token))
               (cons 'int (read-token)))]
      [(char) (begin
                (gen-imm (cdr token))
                (cons 'char (read-token)))]
      ;; FIXME: string concatenation
      [(str) (let ([addr (env-add-string! env (cdr token))])
               (gen-imm addr)
               (cons 'ptr (read-token)))]
      [(sizeof) (begin
                  (unless (token-type=? (read-token) 'group)
                    (error "open paren expected in sizeof"))
                  (let ([expr (read-token)])
                    (cond
                     [(token-type=? expr 'num) (gen-imm (cdr expr))]
                     [(token-type=? expr 'str) (gen-imm (+ (string-length
                                                            (cdr expr))
                                                           1))]
                     [else (error "cannot sizeof " expr)]))
                  (unless (token-type=? (read-token) 'group)
                    (error "close paren expected in sizeof"))
                  (cons 'int (read-token)))]
      [(sym) (let ([sym (hash-table-ref (env-syms env) (cdr token))]
                   [next (read-token)])
               (case (token-type next)
                 [(group) (let ([pop-count
                                 (let loop ([push-count 0]
                                            [param (read-token)])
                                   (if (token-type=? param 'endgroup)
                                       push-count
                                       (let* ([result (expr env 'assign param)]
                                              [type (car result)]
                                              [param (cdr result)])
                                         (gen-op 'psh)
                                         (loop (+ push-count 1)
                                               (if (token-type=? param 'comma)
                                                   (read-token)
                                                   param)))))])
                            (unless sym
                              (error "symbol not found" token))
                            (case (sym-class sym)
                              [(sys) (gen-op (sym-addr sym))]
                              [(func) (begin
                                        (gen-op2 'jsr (sym-addr sym)))]
                              [else (error "bad function call" token)])
                            (if (positive? pop-count)
                                (gen-op2 'adj pop-count))
                            (cons (sym-type sym) (read-token)))]
                 [(assign) (case (sym-class sym)
                             [(local global) (begin
                                               (gen-op2 (if (eq? (sym-class sym) 'local) 'lea 'imm)
                                                        (sym-addr sym))
                                               (gen-op 'psh)
                                               (let ([result (expr env 'assign (read-token))])
                                                 (if (eq? (sym-type sym) 'char)
                                                     (gen-op 'sc)
                                                     (gen-op 'si))
                                                 (cons (sym-type sym) (cdr result))))]
                             [else (error "invalid lvalue" sym)])]
                 [else
                  (case (sym-class sym)
                    [(num) (begin
                             (gen-imm (sym-addr sym))
                             (cons (sym-type sym) next))]
                    [(local global) (begin
                                      (gen-op2 (if (eq? (sym-class sym) 'local) 'lea 'imm)
                                               (sym-addr sym))
                                      (if (eq? (sym-type sym) 'char)
                                          (gen-op 'lc)
                                          (gen-op 'li))
                                      (cons (sym-type sym) next))]
                    [else (error "undefined variable")])]))]
      [(group) (let ([next (read-token)])
                 (case (token-type next)
                   [(int char) (error "func decl")]
                   [else (let* ([ret (expr env 'assign next)]
                                [type (car ret)]
                                [next (cdr ret)])
                           (unless (token-type=? next 'endgroup)
                             (error "close paren expected" next))
                           (cons type (read-token)))]))]
      [(mul) (error "pointer dereference supported")]
      [(and) (error "address retrieval supported")]
      [(not) (let ([result (expr env 'inc (read-token))])
               (gen-op 'psh)
               (gen-imm 0)
               (gen-op 'eq)
               (cons 'int (cdr result)))]
      [(complement) (let ([result (expr env 'inc (read-token))])
                      (gen-op 'psh)
                      (gen-imm -1)
                      (gen-op 'xor)
                      (cons 'int (cdr result)))]
      [(add) (let ([result (expr env 'inc (read-token))])
               (cons 'int (cdr result)))]
      [(sub) (let ([next (read-token)])
               (if (token-type=? next 'num)
                   (begin
                     (gen-imm (- (cdr next)))
                     (cons 'int (read-token)))
                   (begin
                     (gen-imm -1)
                     (gen-op 'psh)
                     (let ([result (expr env 'inc next)])
                       (gen-op 'mul)
                       (cons 'int (cdr result))))))]
      [(inc dec) (error "not supported")]
      [else (error "bad expression" token)]))

  (define (clime-up type op)
    (define (climb? token context)
      (let ([id-a (list-index (cut eq? <> (token-type token)) *precedence-list*)]
            [id-b (list-index (cut eq? <> context) *precedence-list*)])
        (if (eq? id-a #f)
            #f
            (>= id-a id-b))))

    (define (psh-expr-op op next-op)
      (gen-op 'psh)
      (let ([result (expr env next-op (read-token))])
        (gen-op (token-type op))
        (clime-up 'int (cdr result))))

    (define (bxx-expr op next-op)
      (let* ([subenv (%make-env (open-output-string)
                                (env-data env)
                                (env-syms env))]
             [subtype (expr subenv next-op (read-token))]
             [subexpr (port->sexp-list
                       (open-input-string
                        (get-output-string
                         (env-program subenv))))]
             [offset (length subexpr)])
        (gen-op2 op offset)
        (let loop ([e subexpr])
          (if (pair? e)
              (begin
                (write (car e) (env-program env))
                (loop (cdr e)))
              (cons 'int (read-token))))))

    (cond
     [(eof-object? op)
      (cons type (eof-object))]
     [(climb? op context)
      (case (token-type op)
        [(assign) (error "assignment should not be handled here!")]
        [(cond) (error "?: not supported")]
        [(lor) (bxx-expr 'bnz 'land)]
        [(land) (bxx-expr 'bz 'or)]
        [(or) (psh-expr-op op 'xor)]
        [(xor) (psh-expr-op op 'and)]
        [(and) (psh-expr-op op 'eq)]
        [(eq) (psh-expr-op op 'lt)]
        [(ne) (psh-expr-op op 'lt)]
        [(lt) (psh-expr-op op 'shl)]
        [(gt) (psh-expr-op op 'shl)]
        [(le) (psh-expr-op op 'shl)]
        [(ge) (psh-expr-op op 'shl)]
        [(shl) (psh-expr-op op 'add)]
        [(shr) (psh-expr-op op 'add)]

        [(add) (begin
                 (gen-op 'psh)
                 (let ([result (expr env 'mul (read-token))])
                   (if (token-type=? type 'ptr)
                       (error "pointer algorithm not supported"))
                   (gen-op 'add)
                   (clime-up (car result) (cdr result))))]
        [(sub) (begin
                 (gen-op 'psh)
                 (let ([result (expr env 'mul (read-token))])
                   (if (token-type=? type 'ptr)
                       (error "pointer algorithm not supported"))
                   (gen-op 'sub)
                   (clime-up (car result) (cdr result))))]
        [(mul) (psh-expr-op op 'inc)]
        [(div) (psh-expr-op op 'inc)]
        [(mod) (psh-expr-op op 'inc)]
        [(inc dec) (error "inc/dec not supported")]
        [(brak) (error "[] not supported")]
        [else ("compiler error, token:" op)])]
     [else
      (cons type op)]))

  (let ([result (gen-lhs token)])
    (clime-up (car result) (cdr result))))

;;; Read and process all tokens until '}' is met. '}' is also
;;; consumed. Results are in 'env'.
(define (stmt env token)
  (define (token-type token)
    (if (pair? token)
        (car token)
        token))

  (define (token-type=? token type)
    (eq? (token-type token) type))

  (define (token-sym token)
    (if (token-type=? token 'sym)
        (cdr token)
        (token-type token)))

  (define (gen-op env op)
    (write op (env-program env))
    ;; work around the fact that we don't have proper sexp stream,
    ;; outputing two consecutive symbols will have no space between
    ;; them
    (write-char #\space (env-program env)))

  (case (token-sym token)
    [(eos) #t]
    [(end) (error "} can't appear here")]
    [(if) (error "if not supported")]
    [(while) (error "while not supported")]
    [(return) (let ([token (read-token)])
                (unless (token-type=? token 'eos)
                  (let ([result (expr env 'assign token)])
                    (unless (token-type=? (cdr result) 'eos)
                      (error "how come expr does not end with ;?" (cdr result)))))
                (gen-op env 'lev)
                #t)]
    [(begin) ("nested code blocks not supported")]
    [else
     (let ([result (expr env 'assign token)])
       (unless (token-type=? (cdr result) 'eos)
         (error "how come expr does not end with ;?" (cdr result))))]))

;;; Read everything from current input port. Save the results in 'env'.
(define (toplevel env)
  (define (token-type token)
    (if (pair? token)
        (car token)
        token))

  (define (token-type=? token type)
    (eq? (token-type token) type))

  (define (token-sym token)
    (unless (token-type=? token 'sym)
      (error "not a symbol" token))
    (cdr token))

  (define (token-sym=? token sym)
    (and (token-type=? token 'sym)
         (eq? (cdr token) sym)))

  (define (gen-op2 env op val) (write (cons op val) (env-program env)))
  (define (gen-op env op)
    (write op (env-program env))
    ;; work around the fact that we don't have proper sexp stream,
    ;; outputing two consecutive symbols will have no space between
    ;; them
    (write-char #\space (env-program env)))

  (define (define-global! name type)
    (let* ([offset (env-add-int! env 0)]
           [id (make-sym 'global offset type)])
      (hash-table-set! (env-syms env) name id)))

  (define (define-local! env name type local-size)
    (let* ([offset local-size]
           [var-size 4]
           [id (make-sym 'local offset type)])
      (hash-table-set! (env-syms env) name id)
      (+ local-size var-size)))

  (define (define-function! name type)
    (define (free-pc-address env)
      (length
       (port->sexp-list
        (open-input-string
         (get-output-string
          (env-program env))))))

    (define (define-var env token type local-size)
      (let loop ([token token])
        (case (token-type token)
          [(eos end mul) (error "pointer syntax not supported")]
          [(sym) (let ([name (cdr token)])
                   (if (hash-table-exists? (env-syms env) name)
                       (error "duplicate local identifier" name))
                   (let ([next (read-token)])
                     (let ([local-size
                            (define-local! env name type local-size)])
                       (case (token-type next)
                         [(comma) (define-var env (read-token) type local-size)]
                         [(eos) local-size]
                         [else (error "compile error" next)]))))]
          [else (error "bad local declaration")])))

    (define (read-func-stmt env token local-size)
      (gen-op2 env 'ent (/  local-size 4))
      (let loop ([token token])
        (unless (token-type=? token 'end)
          (stmt env token)
          (loop (read-token))))
      (if (positive? local-size)
          (gen-op2 env 'adj (/ local-size 4)))
      (gen-op env 'lev))

    (let* ([free-pc-addr (free-pc-address env)]
           [func-id (make-sym 'function free-pc-addr type)]
           [free-pc-addr (+ free-pc-addr 1)]
           [token (read-token)])
      (hash-table-set! (env-syms env) name func-id)
      [cond
       [(token-type=? token 'endgroup)
        (begin
          (if (token-type=? (read-token) 'begin)
              (let ([funcenv (%make-env (env-program env)
                                        (env-data env)
                                        (hash-table-copy (env-syms env)))])
                (let loop ([token (read-token)]
                           [local-size 0])
                  (if (token-type=? token 'end)
                      (begin
                        (gen-op2 funcenv 'ent 0)
                        (gen-op funcenv 'lev))
                      (cond
                       [(token-sym=? token 'int)
                        (let ([local-size
                               (define-var funcenv (read-token) 'int local-size)])
                          (loop (read-token) local-size))]
                       [(token-sym=? token 'char)
                        (let ([local-size
                               (define-var funcenv (read-token) 'char local-size)])
                          (loop (read-token) local-size))]
                       [else
                        (read-func-stmt funcenv token local-size)]))))
              (error ("expect } after ("))))]]))

  (define (define-1 token type)
    (let loop ([token token])
      (case (token-type token)
        [(eos end mul) (error "pointer syntax not supported")]
        [(sym) (let ([name (cdr token)])
                 (if (hash-table-exists? (env-syms env) name)
                     (error "duplicate global identifier" name))
                 (let ([next (read-token)])
                   (if (token-type=? next 'group)
                       (define-function! name type)
                       (begin
                         (define-global! name type)
                         (case (token-type next)
                           [(comma) (define-1 (read-token) type)]
                           [(eos) (eof-object)]
                           [else (error "compile error" next)])))))]
        [else (error "bad global declaration")])))

  (let read-type ([token (read-token)])
    (unless (eof-object? token)
      (begin
        (case (token-sym token)
          [(int) (define-1 (read-token) 'int)]
          [(char) (define-1 (read-token) 'char)]
          [(enum) (error "not supported")]
          [else
           (error "compile error" token)])
        (read-type (read-token))))))

;;; Jump to the start-th instruction in program and execute until
;;; 'exit' is found.
(define (run program start data :optional (tracer #f))
  (define (op-type token)
    (if (pair? token)
        (car token)
        token))

  (define (push-stack sp value)
    (let ([sp (- sp 4)])
      (s32vector-set! data (/ sp 4) value)
      sp))

  (define (pop-stack sp :optional (offset 4))
    (+ sp offset))

  (define (peek-stack sp)
    (s32vector-ref data (/ sp 4)))

  (define (read-w32 a)
    (unless (= 0 (modulo a 4))
      (error "unaligned access" a))
    (s32vector-ref data (/ a 4)))

  (define (write-w32 a value)
    (unless (= 0 (modulo a 4))
      (error "unaligned access" a))
    (s32vector-set! data (/ a 4) value))

  (let (#;[program (list->vector (port->sexp-list (current-input-port)))]
        [program (list->vector program)])
    (let execute ([pc start]
                  [sp (* (- (s32vector-length data) 1) 4)]
                  [bp 0]
                  [a 0])
      (let* ([op (vector-ref program pc)]
             [next-pc (+ pc 1)]
             [op-sp-a (lambda (expr)
                        (let ([sp (pop-stack sp)]
                              [a (expr (peek-stack sp) a)])
                          (execute next-pc sp bp a)))]
             [op-if-sp-a (lambda (expr)
                           (let ([sp (pop-stack sp)]
                                 [a (if (expr (peek-stack sp) a) 1 0)])
                             (execute next-pc sp bp a)))]
             [neq (lambda (a b) (not (= a b)))]
             [rash (lambda (a b) (ash a (- b)))])
        (if (procedure? tracer)
            (tracer pc op))
        (case (op-type op)
          [(lea) (let ([a (+ bp (* (cdr op) 4))])
                   (execute next-pc sp bp a))]
          [(imm) (let ([a (cdr op)])
                   (execute next-pc sp bp (cdr op)))]
          [(jmp) (let ([next-pc (cdr op)])
                   (execute next-pc sp bp a))]
          [(jsr) (let* ([next-pc (cdr op)]
                        [sp (push-stack sp (+ pc 1))])
                   (execute next-pc sp bp a))]
          [(bz) (let ([next-pc (if (= a 0)
                                   (cdr op)
                                   next-pc)])
                  (execute next-pc sp bp a))]
          [(bnz) (let ([next-pc (if (= a 0)
                                    next-pc
                                    (cdr op))])
                   (execute next-pc sp bp a))]
          [(ent) (let* ([sp (- (push-stack sp bp)
                               (* 4 (cdr op)))]
                        [bp sp])
                   (execute next-pc sp bp a))]
          [(adj) (let ([sp (+ sp (* 4 (cdr op)))])
                   (execute next-pc sp bp a))]
          [(lev) (let ([bp (peek-stack bp)]
                       [next-pc (peek-stack (+ bp 4))]
                       [sp (pop-stack bp 8)])
                   (execute next-pc sp bp a))]
          [(li) (let ([a (read-w32 a)])
                  (execute next-pc sp bp a))]
          [(lc) (let ([a (logand (read-w32 a) #xff)])
                  (execute next-pc sp bp a))]
          [(si) (let ([addr (peek-stack sp)]
                      [sp (pop-stack sp)])
                  (write-w32 addr a)
                  (execute next-pc sp bp a))]
          [(sc) (let* ([addr8 (peek-stack sp)]
                       [addr32 (/ addr8 4)]
                       [shift (* 8 (modulo addr8 4))]
                       [mask (lognot (ash #xff shift))]
                       [old-value (logand (read-w32 (* addr32 4)) mask)]
                       [a (logand a #xff)]
                       [new-value (logior old-value (ash a shift))]
                       [sp (pop-stack sp)])
                  (write-w32 (* addr32 4) new-value)
                  (execute next-pc sp bp a))]
          [(psh) (let ([sp (push-stack sp a)])
                   (execute next-pc sp bp a))]

          [(or)  (op-sp-a logior)]
          [(xor) (op-sp-a logxor)]
          [(and) (op-sp-a logand)]
          [(eq)  (op-if-sp-a =)]
          [(ne)  (op-if-sp-a neq)]
          [(lt)  (op-if-sp-a <)]
          [(gt)  (op-if-sp-a >)]
          [(le)  (op-if-sp-a <=)]
          [(ge)  (op-if-sp-a >=)]
          [(shl) (op-sp-a ash)]
          [(shr) (op-sp-a rash)]
          [(add) (op-sp-a +)]
          [(sub) (op-sp-a -)]
          [(mul) (op-sp-a *)]
          [(div) (op-sp-a /)]
          [(mod) (op-sp-a modulo)]

          [(exit) (peek-stack sp)]
          [else (error "unknown op" op)])))))
