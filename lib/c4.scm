(define-library (c4)
  (export %make-env make-env env-program env-data env-syms
          make-sym sym-class sym-addr sym-type
          read-token expr stmt toplevel run)
  (import (scheme base) (scheme char) (scheme cxr) (scheme write)
          (srfi 1) (srfi 4) (srfi 8) (srfi 26) (srfi 60) (srfi 69)
          (gauche base) (gauche record))
  (include "c4-body.scm"))
